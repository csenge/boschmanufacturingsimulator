package edu.bosch.ManufacturingMachineSimulator.repository;

import edu.bosch.ManufacturingMachineSimulator.model.QualityGateData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcQualityGateDataRepositoryImpl implements QualityGateDataRepository {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(QualityGateData qualityGateData) {
        String sql = "INSERT INTO ManufacturingData " +
                "(part_id, errorCode) VALUES (?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, qualityGateData.getPart_id());
            ps.setInt(2, qualityGateData.getErrorCode());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public QualityGateData findById(int id) {
        String sql = "SELECT * FROM QualityGateData where id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            QualityGateData customer = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer = new QualityGateData(
                        rs.getString("part_id"),
                        rs.getInt("errorCode")

                );
            }
            rs.close();
            ps.close();
            return customer;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }    }
}
