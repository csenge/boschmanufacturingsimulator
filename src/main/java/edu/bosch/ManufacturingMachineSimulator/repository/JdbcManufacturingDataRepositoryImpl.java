package edu.bosch.ManufacturingMachineSimulator.repository;

import edu.bosch.ManufacturingMachineSimulator.model.ManufacturingData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcManufacturingDataRepositoryImpl implements ManufacturingDataRepository {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(ManufacturingData manufacturingData) {
        String sql = "INSERT INTO ManufacturingData " +
                "(id, part_id, batch_id, production_timestamp, tool_id, parameter1, parameter2, upperLimit, lowerLimit) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, manufacturingData.getId());
            ps.setString(2, manufacturingData.getPart_id());
            ps.setTimestamp(3, manufacturingData.getProduction_timestamp());
            ps.setString(4, manufacturingData.getTool_id());
            ps.setFloat(5, manufacturingData.getParameter1());
            ps.setFloat(6, manufacturingData.getParameter2());
            ps.setString(7, manufacturingData.getUpperLimit());
            ps.setString(8, manufacturingData.getLowerLimit());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    @Override
    public ManufacturingData findById(int id) {
        String sql = "SELECT * FROM ManufacturingData where id = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ManufacturingData customer = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer = new ManufacturingData(
                        rs.getInt("id"),
                        rs.getString("part_id"),
                        rs.getString("batch_id"),
                        rs.getTimestamp("production_timestamp"),
                        rs.getString("tool_id"),
                        rs.getFloat("parameter1"),
                        rs.getFloat("parameter2"),
                        rs.getString("upperLimit"),
                        rs.getString("lowerLimit")
                );
            }
            rs.close();
            ps.close();
            return customer;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
}
