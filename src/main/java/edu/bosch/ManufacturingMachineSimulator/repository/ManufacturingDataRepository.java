package edu.bosch.ManufacturingMachineSimulator.repository;

import edu.bosch.ManufacturingMachineSimulator.model.ManufacturingData;

public interface ManufacturingDataRepository {

     void insert(ManufacturingData manufacturingData);
     ManufacturingData findById(int id);
}
