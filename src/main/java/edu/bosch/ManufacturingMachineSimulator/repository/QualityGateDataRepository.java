package edu.bosch.ManufacturingMachineSimulator.repository;

import edu.bosch.ManufacturingMachineSimulator.model.QualityGateData;

public interface QualityGateDataRepository {

    void insert(QualityGateData qualityGateData);
    QualityGateData findById(int id);
}
