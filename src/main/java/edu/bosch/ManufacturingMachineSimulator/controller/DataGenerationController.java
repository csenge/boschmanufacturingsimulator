package edu.bosch.ManufacturingMachineSimulator.controller;

import edu.bosch.ManufacturingMachineSimulator.repository.JdbcManufacturingDataRepositoryImpl;
import edu.bosch.ManufacturingMachineSimulator.repository.JdbcQualityGateDataRepositoryImpl;
import edu.bosch.ManufacturingMachineSimulator.service.DataGenerationService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DataGenerationController {

    private DataGenerationService dataGenerationService;

    @GetMapping
    public void startGeneration() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("Spring-Module.xml");
        dataGenerationService = new DataGenerationService((JdbcManufacturingDataRepositoryImpl) context.getBean("ManufacturingDataRepository"),
                (JdbcQualityGateDataRepositoryImpl) context.getBean("QualityGateDataRepository"));

        try {
            while (true) {
                dataGenerationService.generateManufacturingData();
                Thread.sleep(200);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
