package edu.bosch.ManufacturingMachineSimulator.service;

import edu.bosch.ManufacturingMachineSimulator.model.ManufacturingData;
import edu.bosch.ManufacturingMachineSimulator.model.QualityGateData;
import edu.bosch.ManufacturingMachineSimulator.repository.ManufacturingDataRepository;
import edu.bosch.ManufacturingMachineSimulator.repository.QualityGateDataRepository;

import java.sql.Timestamp;


public class DataGenerationService {


    ManufacturingDataRepository manufacturingDataRepository;

    QualityGateDataRepository qualityGateDataRepository;

    public DataGenerationService(ManufacturingDataRepository manufacturingDataRepository, QualityGateDataRepository qualityGateDataRepository) {
        this.manufacturingDataRepository = manufacturingDataRepository;
        this.qualityGateDataRepository = qualityGateDataRepository;
    }

    private String batchId = "";
    private int count = 0;

    public void generateManufacturingData() {
        ManufacturingData manufacturingData = new ManufacturingData();
        manufacturingData.setPart_id(generateRandomIntLength10());
        //todo consider make this the real deal
       if (batchId.equals("") || count == 200) {
           manufacturingData.setBatch_id(generateRandomIntLength7());
       } else {
           manufacturingData.setBatch_id(batchId);
           count++;
       }
        manufacturingData.setProduction_timestamp(new Timestamp(System.currentTimeMillis()));
       String toolId = Integer.valueOf(manufacturingData.getPart_id().substring(manufacturingData.getPart_id().length() - 2)) % 2 == 0 ? "2" : "1";
       manufacturingData.setTool_id(toolId);
       manufacturingData.setParameter1(0); //todo
       manufacturingData.setParameter2(0); //todo
       manufacturingData.setUpperLimit("0"); //todo
       manufacturingData.setLowerLimit("0"); //todo

       generateGateQualityData(manufacturingData);

       manufacturingDataRepository.insert(manufacturingData);
    }

    private void generateGateQualityData(ManufacturingData manufacturingData) {
        QualityGateData qualityGateData = new QualityGateData();
        qualityGateData.setPart_id(manufacturingData.getPart_id());

        float lowerLimit = Float.parseFloat(manufacturingData.getLowerLimit());
        float upperLimit = Float.parseFloat(manufacturingData.getUpperLimit());

       boolean param1InRange = isInRange(manufacturingData.getParameter1(), lowerLimit, upperLimit);
       boolean param2InRange = isInRange(manufacturingData.getParameter2(), lowerLimit, upperLimit);

       if (!param1InRange && !param2InRange) qualityGateData.setErrorCode(3);
       if (!param1InRange && param2InRange) qualityGateData.setErrorCode(1);
       if (param1InRange && !param2InRange) qualityGateData.setErrorCode(2);
       if (param1InRange && param2InRange) qualityGateData.setErrorCode(0);

       qualityGateDataRepository.insert(qualityGateData);
    }

    private String generateRandomIntLength10(){
        return  (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L + "";
    }

    private String generateRandomIntLength7(){
        return  (long) Math.floor(Math.random() * 9_000_000L) + 1_000_000L + "";
    }

    private boolean isInRange(float value, float min, float max){
        return (value > min && value < max);

    }
}
